----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:39:38 01/09/2016 
-- Design Name: 
-- Module Name:    display - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity display is
    Port ( clk 					: 	in  STD_LOGIC;
           seg_unid				:	in  STD_LOGIC_VECTOR (6 downto 0);
           seg_dec 				: 	in  STD_LOGIC_VECTOR (6 downto 0);
           display_number 		: 	in  STD_LOGIC_VECTOR (6 downto 0);
           display_selection	: 	in  STD_LOGIC_VECTOR (3 downto 0));
end display;

architecture Behavioral of display is
signal control	:STD_LOGIC_VECTOR (1 downto 0);
signal number1: unsigned (display_number'range);
signal number2: unsigned (display_number'range);
begin


	process (clk)
	begin 

	if clk'event and clk='1' then
		display_selection<="0010";
		if seg_dec= "1101101" and seg_unid= "0000000" then
			number2<= seg_dec;
			number2<= number2 -1;
			control <= "00";
		end if;
		
		if seg_dec= "0110000" and seg_unid= "0000000" then 
			number2<= seg_dec;
			number2<= number2 -1;	
			control <= "01";
		end if;
		display_number <=std_logic_vector(number2);
		
	else 
		display_selection<="0001";
		if control = "00" and seg_unid= "0000000" then 
			number1<= 10;
			control<="10";
		end if;
		if seg_dec= "0110000" then 
			number1<= numbe1 -1;
		end if;
		if control= "00" and seg_unid= "0000000" then 
			number1<= 10;
			control<= "11";
		end if;
		if seg_dec= "0000000" then 
			number1<= numbe1 -1;
		end if;
		display_number <=std_logic_vector(number1);
	end if;
	
end process;


	
	
end Behavioral;

